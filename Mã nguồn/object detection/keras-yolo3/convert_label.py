import glob
import os
import argparse
import cv2

COLOR=[(255,0,0),(0,255,0),(0,0,255)]
def convert(input, output):
    imgs_path="/home/aimenext/cuongdx/master_hust/data/train/copy/imgs"
    train_txt="train.txt"
    save="/home/aimenext/cuongdx/master_hust/data/train/viz"
    gt_files=glob.glob(input+"*")
    with open(os.path.join(output,train_txt),"w") as txt_file:
        for file in gt_files:
            print(file)
            new_line=""
            gt_fname=os.path.basename(file)
            img_fname=gt_fname.replace("gt_","").replace("txt","png")
            img_path=os.path.join(imgs_path,img_fname)
            img=cv2.imread(img_path)
            new_line+=img_path
            img_save=os.path.join(save,img_fname)
            with open(file,"r") as gt_file:
                lines=gt_file.readlines()
            for line in lines:
                line=line.replace("\n","")
                line=line.split(",")
                print(line)
                coords=[int(coord) for coord in line[:-1]]
                label=int(line[-1])-1
                xs=[c for c in coords[::2]]
                ys=[c for c in coords[1::2]]
                xmin=min(xs)
                xmax=max(xs)
                ymin=min(ys)
                ymax=max(ys)
                cv2.rectangle(img,(xmin,ymin),(xmax,ymax),COLOR[label],2)
                # cv2.namedWindow("img",cv2.WINDOW_KEEPRATIO)
                # cv2.imshow("img",img)
                cv2.imwrite(img_save,img)
                # cv2.waitKey(0)
                new_line+=" {},{},{},{},{}".format(xmin,ymin,xmax,ymax,label)
            txt_file.write(new_line+"\n")


if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument("--input",type=str,default="/home/aimenext/cuongdx/master_hust/data/train/copy/gt/")
    parser.add_argument("--output",type=str,default="/home/aimenext/cuongdx/master_hust/data/train/copy")
    args=parser.parse_args()
    input=args.input
    output=args.output
    convert(input,output)