from shutil import copyfile
import os
import glob
import argparse

# path="/media/aimenext/Newdisk/cuongdx/hoyu/data/all_img/cate3"
# save="/media/aimenext/Newdisk/cuongdx/hoyu/data/all_img/error/cate3"

def process(input,output,txt_file):
	with open(txt_file,"r") as error_file:
		lines=error_file.readlines()
		for line in lines:
			fname=line.replace("\n","")
			full_path=os.path.join(input,fname)
			files=glob.glob(full_path+"*")
			for file in files:
				base_name=os.path.basename(file)
			new_path=os.path.join(output,base_name)
			copyfile(file,new_path)

def copy_in_txt(output):
    folders = ["cate1", "cate2", "cate3"]
    parent = "/media/aimenext/Newdisk/cuongdx/hoyu/data/all_img"
    list_fname = []
    with open("error.txt", "r") as error_file:
        lines = error_file.readlines()
        for line in lines:
            fname = line.replace("\n", "")
            list_fname.append(fname)
    for folder in folders:
        files = glob.glob(os.path.join(parent, folder) + "/*")
        for file in files:
            base_name = os.path.basename(file)
            if base_name in list_fname:
                new_path = os.path.join(output, base_name)
                copyfile(file, new_path)

def copy_without_in_txt(output):
    folders=["cate1","cate2","cate3"]
    parent="/media/aimenext/Newdisk/cuongdx/hoyu/data/all_img"
    list_fname=[]
    with open("/media/aimenext/Newdisk/cuongdx/hoyu/data/all_img/error.txt", "r") as error_file:
        lines = error_file.readlines()
        for line in lines:
            fname = line.replace("\n", "")
            list_fname.append(fname)
    for folder in folders:
        files=glob.glob(os.path.join(parent,folder)+"/*")
        for file in files:
            base_name=os.path.basename(file)
            if base_name not in list_fname:
            # full_path = os.path.join(input, fname)
            # files = glob.glob(full_path + "*")
            # for file in files:
            #     base_name = os.path.basename(file)
                new_path = os.path.join(output, base_name)
                copyfile(file, new_path)
# copy_in_txt("/media/aimenext/Newdisk/cuongdx/hoyu/data/all_img/error_yolo")
# process("/media/aimenext/Newdisk/cuongdx/hoyu/data/hoyu_photograph/yolo/cate3","/media/aimenext/Newdisk/cuongdx/hoyu/data/hoyu_photograph/pl_crnn/cate3/train/imgs","error.txt")

# if __name__ == '__main__':
#     parser=argparse.ArgumentParser()
#     parser.add_argument("--input",type=str)
#     parser.add_argument("--output",type=str)
#     parser.add_argument("--txt",type=str)
#
#     args=parser.parse_args()
#     input=args.input
#     output=args.output
#     txt_file=args.txt
#     process(input,output,txt_file)

process("/home/aimenext/cuongdx/master_hust/data/train/gt","/home/aimenext/cuongdx/master_hust/data/train/copy/gt","error.txt")