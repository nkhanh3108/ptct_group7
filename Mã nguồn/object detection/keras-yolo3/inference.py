import argparse
from yolo import YOLO
from PIL import Image
import os
import glob
import cv2
import numpy as np

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

font = cv2.FONT_HERSHEY_SIMPLEX
# fontScale
fontScale = 0.5
# Blue color in BGR
# color = (0, 0, 255)
# Line thickness of 2 px
thickness = 1
OUTPUT_SHAPE = (1044, 2224)
WIDTH_RECT = [2224]
HEIGHT_RECT = []
MAX_EXPAND = 10
COLOR=[(255,0,0),(0,255,0),(0,0,255)]
dict_cls={"page title":0,"widget label":1,"error message":2}

def non_max_suppression_fast(boxes, labels, overlapThresh):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")
    #
    # initialize the list of picked indexes
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)

    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))

    # return only the bounding boxes that were picked using the
    # integer data type

    final_labels = [labels[idx] for idx in pick]
    final_boxes = boxes[pick].astype("int")

    return final_boxes, final_labels


def load_model(model_path):
    yolo = YOLO(model_path)
    return yolo


def get_center_point(box):
    xmin, ymin, xmax, ymax = box
    return (xmin + xmax) // 2, (ymin + ymax) // 2


def perspective_transform(image, source_points, output_shape=OUTPUT_SHAPE):
    height, width = output_shape
    dest_points = np.float32([[0, 0], [width - 1, 0], [width - 1, height - 1], [0, height - 1]])
    M = cv2.getPerspectiveTransform(source_points, dest_points)
    dst = cv2.warpPerspective(image, M, (width, height))

    return dst


def expand_rect(label_boxes, width, height):
    top_left = label_boxes['top_left']
    top_right = label_boxes['top_right']
    bot_right = label_boxes['bot_right']
    bot_left = label_boxes['bot_left']
    print(label_boxes)
    max_expand = 10
    # top left
    x_topleft, y_topleft = top_left
    if x_topleft < max_expand:
        max_expand = x_topleft
    if y_topleft < max_expand:
        max_expand = y_topleft

    # top right
    x_topright, y_topright = top_right
    if x_topright + max_expand >= width:
        max_expand = width - x_topright - 1
    if y_topright < max_expand:
        max_expand = y_topright

    # bot right
    x_botright, y_botright = bot_right
    if x_botright + max_expand >= width:
        max_expand = width - 1 - x_botright
    if y_topright + max_expand >= height:
        max_expand = height - 1 - y_topright

    # bot left
    x_botleft, y_botleft = bot_left
    if x_botleft < max_expand:
        max_expand = x_botleft
    if y_botleft + max_expand >= height:
        max_expand = height - 1 - y_botleft

    new_dict = {}
    print("expandddddddddddd: ", max_expand)
    new_dict["top_left"] = (x_topleft - max_expand, y_topleft - max_expand)
    new_dict["top_right"] = (x_topright + max_expand, y_topright - max_expand)
    new_dict["bot_right"] = (x_botright + max_expand, y_botright + max_expand)
    new_dict["bot_left"] = (x_botleft - max_expand, y_botleft + max_expand)
    return new_dict, max_expand


def find_more_point(label_boxes, width):
    if len(label_boxes) == 3:
        if "top_left" not in label_boxes:
            y_topright = label_boxes["top_right"][1]
            x_botleft = label_boxes["bot_left"][0]
            label_boxes["top_left"] = (0, y_topright)
        elif "top_right" not in label_boxes:
            y_topleft = label_boxes["top_left"][1]
            x_botright = label_boxes["bot_right"][0]
            label_boxes["top_right"] = (width - 1, y_topleft)
        elif "bot_right" not in label_boxes:
            y_botleft = label_boxes["bot_left"][1]
            x_topright = label_boxes["top_right"][0]
            label_boxes["bot_right"] = (width - 1, y_botleft)
        elif "bot_left" not in label_boxes:
            y_botright = label_boxes["bot_right"][1]
            x_topleft = label_boxes["top_left"][0]
            label_boxes["bot_left"] = (0, y_botright)
    elif len(label_boxes) == 2:
        if "top_left" in label_boxes and "bot_right" in label_boxes:
            x_topleft, y_topleft = label_boxes["top_left"]
            x_botright, y_botright = label_boxes["bot_right"]
            label_boxes["top_right"] = (width - 1, y_topleft)
            label_boxes["bot_left"] = (0, y_botright)
        elif "top_right" in label_boxes and "bot_left" in label_boxes:
            x_topright, y_topright = label_boxes["top_right"]
            x_botleft, y_botleft = label_boxes["bot_left"]
            label_boxes["top_left"] = (0, y_topright)
            label_boxes["bot_right"] = (width - 1, y_topright)
        elif "top_left" in label_boxes and "bot_left" in label_boxes:
            y_topleft = label_boxes["top_left"][1]
            y_botleft = label_boxes["bot_left"][1]
            label_boxes["top_right"] = (width - 1, y_topleft)
            label_boxes["bot_right"] = (width - 1, y_botleft)
        elif "top_right" in label_boxes and "bot_right" in label_boxes:
            y_topright = label_boxes["top_right"][1]
            y_botright = label_boxes["bot_right"][1]
            label_boxes["top_left"] = (0, y_topright)
            label_boxes["bot_left"] = (0, y_botright)

    return label_boxes


def get_box_max_score(list_boxes, classes):
    final_boxes = dict.fromkeys(list(set(classes)))
    for box, cls in zip(list_boxes, classes):
        if final_boxes[cls] is None:
            final_boxes[cls] = box
        elif float(final_boxes[cls]['score']) < float(box['score']):
            final_boxes[cls] = box

    return list(final_boxes.values()), list(final_boxes.keys())


def show_image(name, mat):
    cv2.namedWindow(name, cv2.WINDOW_KEEPRATIO)
    cv2.imshow(name, mat)
    cv2.waitKey()


def detect_img(yolo, img, visualize=True):
    rects, classes = yolo.detect_image_new(img)
    print(rects, classes)
    img=np.array(img,dtype=np.uint8)
    # for rect in rects:
    #     y1 = float(rect["y1"])
    #     y2 = float(rect["y2"])
    #     x1 = float(rect["x1"])
    #     x2 = float(rect["x2"])
    #     cv2.rectangle(img, (int(x1), int(y1)), (int(x2), int(y2)), (0, 0, 255), 2)
    # show_image("img",img)
    # rects, classes = get_box_max_score(rects, classes)
    img = np.array(img, dtype=np.uint8)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    height, width = img.shape[:2]
    boxes = []
    if rects == [] or classes == []:
        return img
    for rect, cls in zip(rects, classes):
        x1, y1, x2, y2 = int(float(rect["x1"])), int(float(rect["y1"])), int(float(rect["x2"])), int(float(rect["y2"]))
        boxes.append([x1, y1, x2, y2])
        # print(x1,y2,x2,y2)

    final_boxes, final_labels = non_max_suppression_fast(np.array(boxes), classes, 0.15)
    if visualize:
        for rect, cls in zip(final_boxes, final_labels):
            # print(rect)
            x1, y1, x2, y2 = rect
            cls_idx=dict_cls[cls]
            print("class: ",cls)
            cv2.rectangle(img, (x1, y1), (x2, y2), COLOR[cls_idx], 2)
            cv2.putText(img, cls, (x2+5, y1+10), font,
                        fontScale, COLOR[cls_idx], thickness, cv2.LINE_AA)

    # final_points = list(map(get_center_point, final_boxes))
    # label_boxes = dict(zip(final_labels, final_points))
    # if len(label_boxes) < 4:
    #     label_boxes = find_more_point(label_boxes, width)
    # if len(label_boxes) == 4 and not visualize:
    #     # print(label_boxes["top_right"][0]-label_boxes["top_left"][0],label_boxes["bot_right"][0]-label_boxes["bot_left"][0])
    #     label_boxes, expand = expand_rect(label_boxes, width, height)
    #     source_points = np.float32([
    #         label_boxes['top_left'], label_boxes['top_right'], label_boxes['bot_right'], label_boxes['bot_left']
    #     ])
    #
    #     # Transform
    #     img = perspective_transform(img, source_points)
    #     if expand < MAX_EXPAND:
    #         exp = MAX_EXPAND - expand
    #         img_h, img_w = img.shape[:2]
    #         big_img = np.ones((img_h + 2 * exp, img_w + 2 * exp, 3), np.uint8) * 255
    #         big_img[exp:-exp, exp:-exp] = img
    #         img = cv2.resize(big_img, (img_w, img_h))
    return img


def detect(yolo, imgs_path, save_path):
    files = glob.glob(os.path.join(imgs_path, "*"))
    for file in files:
        print(file)
        fname = os.path.basename(file)
        img = Image.open(file)
        result = detect_img(yolo, img, True)
        cv2.imwrite(os.path.join(save_path, fname), np.array(result, np.uint8))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--imgs_path", type=str, default="../data/test/input")
    parser.add_argument("--save_path", type=str, default="../data/test/output")
    parser.add_argument("--model_path", type=str, default="../models/ep1870-loss28.783-val_loss24.822.h5")

    args = parser.parse_args()
    imgs_path = args.imgs_path
    save_path = args.save_path
    model_path = args.model_path
    yolo = load_model(model_path)
    detect(yolo, imgs_path, save_path)
