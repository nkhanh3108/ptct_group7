import cv2
import numpy as np
import os
import glob


path="/home/cuongdx/Pictures/sic/11_13/all"
imgs_path=os.path.join(path,"imgs")
gts_path=os.path.join(path,"gt")
imgs_file=glob.glob(imgs_path+"/*")

def show_img(name,mat):
	cv2.namedWindow(name,cv2.WINDOW_KEEPRATIO)
	cv2.imshow(name,mat)
	cv2.waitKey()

def order_points(rotated_box):
	points = cv2.boxPoints(rotated_box)
	size=rotated_box[1]
	angle=rotated_box[2]
	width, height = size
	if abs(angle+90)<1e-4:
		order=[2,3,0,1]
	elif abs(angle)<1e-4:
		order=[1,2,3,0]
	elif height>width:
		order=[2,3,0,1]
	else:
		order=[1,2,3,0]

	new_order= [points[i] for i in order]
	return np.int32(new_order)

for file in imgs_file:
	img_fname=os.path.basename(file)
	gt_fname="gt_"+img_fname.replace("jpg","txt")
	img=cv2.imread(file)
	h,w=img.shape[:2]

	with open(os.path.join(gts_path,gt_fname),"r") as gt_file:
		lines=gt_file.readlines()
	for line in lines:
		blank_img = np.ones((h, w), np.uint8) * 255
		line=line.rstrip()
		line=line.split(",")
		lbl=line[-1]
		coord=[int(coor) for coor in line[:-1]]
		contour=np.array(coord,dtype=np.int32).reshape(4,2)
		cv2.polylines(blank_img,[contour],True,0)
		cnts=np.where(blank_img==0)
		cnts=np.array([[x,y] for (y,x) in zip(cnts[0],cnts[1])])
		rect_box=cv2.minAreaRect(cnts)
		points=cv2.boxPoints(rect_box)
		print(points,rect_box)
		new_points=order_points(rect_box)
		# cv2.polylines(img, [np.int32(points)], True, 255)
		for point in new_points:
			cv2.circle(img,tuple(point),5,255,5)
			show_img("img",img)