"""
Retrain the YOLO model for your own dataset.
"""

import numpy as np
import keras.backend as K
from keras.layers import Input, Lambda
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau, EarlyStopping

from yolo3.model import preprocess_true_boxes, yolo_body, tiny_yolo_body, yolo_loss
from yolo3.utils import get_random_data
from datetime import datetime
import os
from argparse import ArgumentParser
import cv2


def load_data(path_train, path_val):
    data_train = []
    data_val = []
    for path in path_train:
        with open(os.path.join(path, "train.txt"), "r") as file_train:
            data_train.extend(file_train.readlines())
    for path in path_val:
        with open(os.path.join(path, "val.txt"), "r") as file_train:
            data_val.extend(file_train.readlines())

    return data_train, data_val


def _main(weights_path, init_epoch, lr):
    train_path = ["../data/train/copy"]
    val_path = ["../data/train/copy"]
    # weights_path="/home/aimenext/cuongdx/keras-yolo3/models/2020_08_04_19h_29m/ep018-loss12.831-val_loss11.946.h5"
    now = datetime.now()
    print(weights_path, init_epoch, lr)
    date_time = now.strftime("%Y_%m_%d_%Hh_%Mm")
    models_save_path = '../models/{}/'.format(date_time)
    if not os.path.exists(models_save_path):
        os.mkdir(models_save_path)
    classes_path = 'model_data/component_class.txt'
    anchors_path = 'model_data/yolo_anchors_0126.txt'
    class_names = get_classes(classes_path)
    num_classes = len(class_names)
    anchors = get_anchors(anchors_path)

    input_shape = (608, 608)  # multiple of 32, hw

    is_tiny_version = len(anchors) == 6  # default setting
    if is_tiny_version:
        model = create_tiny_model(input_shape, anchors, num_classes,
                                  freeze_body=2, weights_path='model_data/tiny_yolo_weights.h5')
    else:
        model = create_model(input_shape, anchors, num_classes, load_pretrained=True,
                             freeze_body=2, weights_path=weights_path)  # make sure you know what you freeze

    logging = TensorBoard(log_dir=models_save_path)
    checkpoint = ModelCheckpoint(models_save_path + 'ep{epoch:03d}-loss{loss:.3f}-val_loss{val_loss:.3f}.h5',
                                 monitor='val_loss', save_weights_only=True, save_best_only=True, period=3)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.8, patience=5, verbose=1)
    early_stopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=1)

    # with open(train_path) as t_f:
    #     train_lines = t_f.readlines()
    # with open(val_path) as t_f:
    #     val_lines = t_f.readlines()
    train_lines, val_lines = load_data(train_path, val_path)
    # np.random.seed(10101)
    # np.random.shuffle(train_lines)
    # np.random.seed(None)
    # v_lines = t_lines[8000:]
    # t_lines = t_lines[:8000]
    num_train = len(train_lines)
    # with open(val_path) as v_f:
    #     v_lines = v_f.readlines()
    # np.random.seed(10010)
    # np.random.shuffle(v_lines)
    # np.random.seed(None)
    num_val = len(val_lines)

    # Train with frozen layers first, to get a stable loss.
    # Adjust num epochs to your dataset. This step is enough to obtain a not bad model.
    # if True:
    #     model.compile(optimizer=Adam(lr=1e-3), loss={
    #         # use custom yolo_loss Lambda layer.
    #         'yolo_loss': lambda y_true, y_pred: y_pred})
    #
    #     batch_size = 4
    #     print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))
    #     model.fit_generator(data_generator_wrapper(train_lines, batch_size, input_shape, anchors, num_classes),
    #                         steps_per_epoch=max(1, num_train // batch_size),
    #                         validation_data=data_generator_wrapper(val_lines, batch_size, input_shape, anchors, num_classes),
    #                         validation_steps=max(1, num_val // batch_size),
    #                         epochs=50,
    #                         initial_epoch=0,
    #                         callbacks=[logging, checkpoint])
    #     model.save_weights(models_save_path + 'trained_weights_stage_1.h5')

    # Unfreeze and continue training, to fine-tune.
    # Train longer if the result is not good.
    if True:
        for i in range(len(model.layers)):
            model.layers[i].trainable = True
        model.compile(optimizer=Adam(lr=lr, amsgrad=True), loss={'yolo_loss': lambda y_true, y_pred: y_pred})  # recompile to apply the change
        print('Unfreeze all of the layers.')

        batch_size = 4  # note that more GPU memory is required after unfreezing the body
        print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))
        model.fit_generator(data_generator_wrapper(train_lines, batch_size, input_shape, anchors, num_classes),
                            steps_per_epoch=max(1, num_train // batch_size),
                            validation_data=data_generator_wrapper(val_lines, batch_size, input_shape, anchors, num_classes),
                            validation_steps=max(1, num_val // batch_size),
                            epochs=2000,
                            initial_epoch=init_epoch,
                            callbacks=[logging,checkpoint, reduce_lr])
        model.save_weights(models_save_path + 'trained_weights_final.h5')

    # Further training if needed.


def get_classes(classes_path):
    '''loads the classes'''
    with open(classes_path) as f:
        class_names = f.readlines()
    class_names = [c.strip() for c in class_names]
    return class_names


def get_anchors(anchors_path):
    '''loads the anchors from a file'''
    with open(anchors_path) as f:
        anchors = f.readline()
    anchors = [float(x) for x in anchors.split(',')]
    return np.array(anchors).reshape(-1, 2)


def create_model(input_shape, anchors, num_classes, load_pretrained=True, freeze_body=2,
                 weights_path='model_data/yolo_weights.h5'):
    '''create the training model'''
    K.clear_session()  # get a new session
    image_input = Input(shape=(None, None, 3))
    h, w = input_shape
    num_anchors = len(anchors)

    y_true = [Input(shape=(h // {0: 32, 1: 16, 2: 8}[l], w // {0: 32, 1: 16, 2: 8}[l], \
                           num_anchors // 3, num_classes + 5)) for l in range(3)]

    model_body = yolo_body(image_input, num_anchors // 3, num_classes)
    print('Create YOLOv3 model with {} anchors and {} classes.'.format(num_anchors, num_classes))

    if load_pretrained:
        model_body.load_weights(weights_path, by_name=True, skip_mismatch=True)
        print('Load weights {}.'.format(weights_path))
        if freeze_body in [1, 2]:
            # Freeze darknet53 body or freeze all but 3 output layers.
            num = (185, len(model_body.layers) - 3)[freeze_body - 1]
            for i in range(num): model_body.layers[i].trainable = False
            print('Freeze the first {} layers of total {} layers.'.format(num, len(model_body.layers)))

    model_loss = Lambda(yolo_loss, output_shape=(1,), name='yolo_loss',
                        arguments={'anchors': anchors, 'num_classes': num_classes, 'ignore_thresh': 0.5})(
        [*model_body.output, *y_true])
    model = Model([model_body.input, *y_true], model_loss)

    return model


def create_tiny_model(input_shape, anchors, num_classes, load_pretrained=True, freeze_body=2,
                      weights_path='model_data/tiny_yolo_weights.h5'):
    '''create the training model, for Tiny YOLOv3'''
    K.clear_session()  # get a new session
    image_input = Input(shape=(None, None, 3))
    h, w = input_shape
    num_anchors = len(anchors)

    y_true = [Input(shape=(h // {0: 32, 1: 16}[l], w // {0: 32, 1: 16}[l], \
                           num_anchors // 2, num_classes + 5)) for l in range(2)]

    model_body = tiny_yolo_body(image_input, num_anchors // 2, num_classes)
    print('Create Tiny YOLOv3 model with {} anchors and {} classes.'.format(num_anchors, num_classes))

    if load_pretrained:
        model_body.load_weights(weights_path, by_name=True, skip_mismatch=True)
        print('Load weights {}.'.format(weights_path))
        if freeze_body in [1, 2]:
            # Freeze the darknet body or freeze all but 2 output layers.
            num = (20, len(model_body.layers) - 2)[freeze_body - 1]
            for i in range(num): model_body.layers[i].trainable = False
            print('Freeze the first {} layers of total {} layers.'.format(num, len(model_body.layers)))

    model_loss = Lambda(yolo_loss, output_shape=(1,), name='yolo_loss',
                        arguments={'anchors': anchors, 'num_classes': num_classes, 'ignore_thresh': 0.7})(
        [*model_body.output, *y_true])
    model = Model([model_body.input, *y_true], model_loss)

    return model

COLOR=[(255,0,0),(0,255,0),(0,0,255)]

def data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes):
    '''data generator for fit_generator'''
    n = len(annotation_lines)
    i = 0
    while True:
        image_data = []
        box_data = []
        for b in range(batch_size):
            if i == 0:
                np.random.shuffle(annotation_lines)
            image, box = get_random_data(annotation_lines[i], input_shape, random=True)
            image_data.append(image)
            # img = np.array(image*255,dtype=np.uint8)
            # for b in box:
            #     xmin,ymin,xmax,ymax,lb=b
            #     cv2.rectangle(img,(int(xmin),int(ymin)),(int(xmax),int(ymax)),COLOR[int(lb)],2)
            # print(img.shape)
            # show_image("img",img)
            box_data.append(box)
            i = (i + 1) % n
        image_data = np.array(image_data)
        box_data = np.array(box_data)
        y_true = preprocess_true_boxes(box_data, input_shape, anchors, num_classes)
        yield [image_data, *y_true], np.zeros(batch_size)


def show_image(name, mat):
    cv2.namedWindow(name, cv2.WINDOW_KEEPRATIO)
    cv2.imshow(name, mat)
    cv2.waitKey()


def data_generator_wrapper(annotation_lines, batch_size, input_shape, anchors, num_classes):
    n = len(annotation_lines)
    if n == 0 or batch_size <= 0: return None
    return data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--weights_path", type=str, default="yolo3/yolo_weights_608.h5")
    parser.add_argument("--init_epoch", type=int, default=1)
    parser.add_argument("--lr", type=float, default=5e-5)
    args = parser.parse_args()
    _main(**vars(args))
