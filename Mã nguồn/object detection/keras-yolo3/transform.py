import numpy as np
import cv2
from math import  exp
DISTANCE_RATIO = 4
import os
from scipy.spatial import distance as dist

def order_points(pts):
    # sort the points based on their x-coordinates
    ySorted = pts[np.argsort(pts[:, 1]), :]
    # grab the left-most and right-most points from the sorted
    # x-roodinate points
    leftMost = ySorted[:2, :]
    rightMost = ySorted[2:, :]
    # now, sort the left-most coordinates according to their
    # y-coordinates so we can grab the top-left and bottom-left
    # points, respectively
    leftMost = leftMost[np.argsort(leftMost[:, 0]), :]
    (tl, tr) = leftMost
    rightMost = rightMost[np.argsort(rightMost[:, 0]), :]
    (bl, br) = rightMost

    return np.array([tl, tr, br, bl], dtype="float32")

def order_points_1(pts):
    # sort the points based on their x-coordinates
    ySorted = pts[np.argsort(pts[:, 1]), :]
    # grab the left-most and right-most points from the sorted
    # x-roodinate points
    leftMost = ySorted[:2, :]
    rightMost = ySorted[2:, :]
    # now, sort the left-most coordinates according to their
    # y-coordinates so we can grab the top-left and bottom-left
    # points, respectively
    leftMost = leftMost[np.argsort(leftMost[:, 0]), :]
    (tl, tr) = leftMost
    rightMost=rightMost[np.argsort(rightMost[:, 0]), :]
    (bl,br)=rightMost

    return np.array([tl, tr, br, bl], dtype="float32")

def order_points_2(pts):
    # sort the points based on their x-coordinates
    xSorted = pts[np.argsort(pts[:, 0]), :]
    # grab the left-most and right-most points from the sorted
    # x-roodinate points
    leftMost = xSorted[:2, :]
    rightMost = xSorted[2:, :]
    # now, sort the left-most coordinates according to their
    # y-coordinates so we can grab the top-left and bottom-left
    # points, respectively
    leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
    (tl, bl) = leftMost
    # now that we have the top-left coordinate, use it as an
    # anchor to calculate the Euclidean distance between the
    # top-left and right-most points; by the Pythagorean
    # theorem, the point with the largest distance will be
    # our bottom-right point
    ed_0 = np.linalg.norm(tl - rightMost[0])
    ed_1 = np.linalg.norm(tl - rightMost[1])
    D=[ed_0,ed_1]
    (br, tr) = rightMost[np.argsort(D)[::-1], :]
    # return the coordinates in top-left, top-right,
    # bottom-right, and bottom-left order
    return np.array([tl, tr, br, bl], dtype="float32")

def order_points_3(pts):
    rect = np.zeros((4, 2), dtype="float32")
    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect

def order_points_4(rotated_box):
	points = cv2.boxPoints(rotated_box)
	size=rotated_box[1]
	angle=rotated_box[2]
	width, height = size
	if abs(angle+90)<1e-4:
		order=[2,3,0,1]
	elif abs(angle)<1e-4:
		order=[1,2,3,0]
	elif height>width:
		order=[2,3,0,1]
	else:
		order=[1,2,3,0]

	new_order= [points[i] for i in order]
	return np.float32(new_order)


def gen_gau_heatmap(score_map_shape, box, distanceRatio):
    mask = np.zeros(score_map_shape, dtype=np.float32)
    scaleGaussian = lambda x: exp(-(1 / 2) * (x ** 2))
    x, y, w, h = box
    heat = np.zeros((h, w), np.float32)
    center = h // 2
    start = h // 4
    # print("x,y,w,h: ",box,distanceRatio)
    for i in range(center):
        distanFromCenter = abs(i - center // 2)
        distanFromCenter = distanceRatio * distanFromCenter / center
        # scaleGaussianProb=scaleGaussian(distanFromCenter)
        scaleGaussianProb = exp(-(1 / 2) * (distanFromCenter ** 2))
        # print("scale: ",scaleGaussianProb,distanFromCenter)
        heat[i + start, :] = np.clip(scaleGaussianProb, 0, 1)

    mask[y:y + h, x:x + w] = heat
    # print("mask: ",np.unique(mask))
    return mask

def gen_gau_heatmap_with_rotated_box( box, distanceRatio):
    # print("box: ", box)
    (tl, tr, br, bl) = box
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))

    # ...and now for the height of our new image
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))

    # take the maximum of the width and height values to reach
    # our final dimensions
    maxWidth = max(int(widthA), int(widthB))
    maxHeight = max(int(heightA), int(heightB))
    if maxHeight > maxWidth * 1.5:
        width = maxHeight
        height = maxWidth
        dst = np.array([
            [width - 1, 0],
            [width - 1, height - 1],
            [0, height - 1],
            [0, 0]], dtype="float32")
    else:
        width = maxWidth
        height = maxHeight
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")

    # calculate the perspective transform matrix and warp
    # the perspective to grab the screen
    M = cv2.getPerspectiveTransform(dst, box)
    maxHeight = height
    maxWidth = width
    bbox = [0, 0, maxWidth, maxHeight]
    # print("shape of gau: ",maxHeight,maxWidth)
    gau=gen_gau_heatmap((maxHeight,maxWidth,),bbox,distanceRatio)
    return gau,M

def transform(score_map_shape,box, distanceRatio):
    # blank_img=np.ones((score_map_shape),dtype=np.uint8)*255
    # contour = np.array(box, dtype=np.int32).reshape(4, 2)
    # cv2.polylines(blank_img, [contour], True, 0)
    # cnts = np.where(blank_img == 0)
    # cnts = np.array([[x, y] for (y, x) in zip(cnts[0], cnts[1])])
    # rect_box = cv2.minAreaRect(cnts)
    # box_0=order_points_4(rect_box)

    box = np.array(box, dtype=np.float32)
    box_0 = order_points_1(box)
    # print(box)
    gau, M=gen_gau_heatmap_with_rotated_box(box_0,distanceRatio)
    print(gau.shape,M)
    transformed = cv2.warpPerspective(gau, M, (score_map_shape[1], score_map_shape[0]))
    bin_img=np.where(transformed>0,255,0)
    _, cnts, _ = cv2.findContours(np.uint8(bin_img), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    nb_cnts=len(cnts)
    # show_mat("before",np.uint8(bin_img))
    # print(nb_cnts)
    if nb_cnts>1:
        box_1 = order_points_3(box)
        gau, M = gen_gau_heatmap_with_rotated_box(box_1, distanceRatio)
        # print(gau.shape, M)
        transformed = cv2.warpPerspective(gau, M, (score_map_shape[1], score_map_shape[0]))
        bin_img = np.where(transformed > 0, 255, 0)
        _, cnts, _ = cv2.findContours(np.uint8(bin_img), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        nb_cnts = len(cnts)
        # show_mat("after", transformed)
        if nb_cnts>1:
            box_2 = order_points_2(box)
            gau, M = gen_gau_heatmap_with_rotated_box(box_2, distanceRatio)
            transformed = cv2.warpPerspective(gau, M, (score_map_shape[1], score_map_shape[0]))
            # show_mat("after2", transformed)
            bin_img = np.where(transformed > 0, 255, 0)
            _, cnts, _ = cv2.findContours(np.uint8(bin_img), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            nb_cnts = len(cnts)
            if nb_cnts>1:
                return np.zeros_like(transformed)

    return transformed

def show_mat(name,mat):
    cv2.namedWindow(name, cv2.WINDOW_KEEPRATIO)
    cv2.imshow(name, mat)
    cv2.waitKey(0)

if __name__ == '__main__':
    # img=np.zeros((100,100),dtype=np.uint8)
    # box=[[20,20],[60,20],[60,40],[20,40]]
    # # box=[[20,20],[40,20],[40,60],[20,60]]
    # # box=[[30,30],[15,50],[70,80],[90,50]]
    # box=[[906,1190],[917,1218],[706,1376],[686,1364]]
    # shape=(2000,2000)
    # mat=gen_gau_heatmap_with_rotated_box(shape,box,DISTANCE_RATIO)
    # show_mat("src",mat)
    import glob
    path_gt= '/media/aimenext/Newdisk/cuongdx/pixellink/data/data/sci/sci_20k_synth_color_1117/0016/gt'
    path_imgs= '/media/aimenext/Newdisk/cuongdx/pixellink/data/data/sci/sci_20k_synth_color_1117/0016/imgs'
    save="/media/aimenext/Newdisk/cuongdx/pixellink/data/data/sci/sci_20k_synth_color_1117/0016/central"
    if not os.path.exists(save):
        os.mkdir(save)

    # path_gt= '/media/aimenext/Newdisk/cuongdx/pixellink/data/data/sci/color_1113/error/augment/gt'
    # path_imgs= '/media/aimenext/Newdisk/cuongdx/pixellink/data/data/sci/color_1113/error/augment/imgs'
    # save="/media/aimenext/Newdisk/cuongdx/pixellink/data/data/sci/color_1113/error/augment/central"

    gt_file=glob.glob(path_gt+"/*16387*")
    for file in gt_file:
        gt_fname=os.path.basename(file)
        print(gt_fname)
        img_fname=gt_fname.replace("gt_","").replace("txt","jpg")
        img=cv2.imread(os.path.join(path_imgs,img_fname))
        height,width=img.shape[:2]
        shape=(height//2,width//2)
        new_img=np.zeros(shape,np.uint8)
        mat=np.zeros(shape,np.float32)
        src=np.zeros(shape,np.uint8)
        with open(file,"r") as gt_file:
            lines=gt_file.readlines()
        for line in lines:
            line=line.replace("\n","").split(",")
            coor=[int(c)//2 for c in line[:-1]]
            coor=np.array(coor,np.int32).reshape(4,2)
            # box=order_points(coor)

            # rot_rect=cv2.minAreaRect(coor)
            # box=get_corner_rot_rect(rot_rect)
            # for point in box:
            #     cv2.circle(img,tuple(point),5,(0,0,255),5)
            #     show_mat("img",img)
            # print(box)
            cv2.fillPoly(src,[coor],(255))
            # show_mat("src", src)
            mat+=transform(shape,coor,DISTANCE_RATIO)
            mat=np.clip(mat,0,1)
        mat=np.array(mat*255,np.uint8)
        # show_mat("gau", mat)
        # show_mat("src",src)
        cv2.imwrite(os.path.join(save,img_fname),mat)