from keras.preprocessing.sequence import pad_sequences
from keras.layers import Embedding, LSTM, Dense, Dropout
from keras.preprocessing.text import Tokenizer
from keras.callbacks import EarlyStopping
from keras.models import Sequential
import keras.utils as ku

# set seeds for reproducability
# from tensorflow import set_random_seed
from numpy.random import seed
# set_random_seed(2)
# seed(1)

import pandas as pd
import numpy as np
import string, os
import pickle

import warnings

warnings.filterwarnings("ignore")
warnings.simplefilter(action='ignore', category=FutureWarning)
PATH = "train_sequences.txt"
import random


# tokenizer = Tokenizer()

def clean_text(txt):
    txt = "".join(v for v in txt).lower()
    txt = txt.encode("utf8").decode("ascii", 'ignore')
    return txt


def load_data_train(path):
    rs = []
    with open(path, "r") as file:
        data = file.readlines()
    for line in data:
        line = line.rstrip().replace("\n", "")
        rs.append(line)
    return rs


def process_data(data):
    dict_word = {}
    dict_index = {}
    idx = 0
    for line in data:
        line = line.split(" ")
        for el in line:
            if el not in dict_word:
                dict_word[el] = idx
                dict_index[idx] = el
                idx += 1

    return dict_word, dict_index


def line_to_index(line, dict_word):
    rs = []
    for el in line:
        rs.append(dict_word[el])
    return rs


def index_to_str(index, dict_index):
    rs = ""
    for el in index:
        rs += dict_index[el] + " "
    return rs.rstrip().upper()


def get_sequence_of_tokens(corpus, dict_word):
    total_words = len(dict_word)
    input_sequences = []
    for line in corpus:
        line = line.split(" ")
        token_list = line_to_index(line, dict_word)
        for i in range(1, len(token_list)):
            n_gram_sequence = token_list[:i + 1]
            input_sequences.append(n_gram_sequence)
    return input_sequences, total_words


def generate_padded_sequences(input_sequences, total_words):
    max_sequence_len = max([len(x) for x in input_sequences])
    input_sequences = np.array(pad_sequences(input_sequences, maxlen=max_sequence_len, padding='pre'))

    predictors, label = input_sequences[:, :-1], input_sequences[:, -1]
    label = ku.to_categorical(label, num_classes=total_words)
    return predictors, label, max_sequence_len


def prin_data_train(input_sequences,dict_index):
    for seq in input_sequences:
        print("Input Sentence: {}, Prediction: {}".format(index_to_str(seq[:-1],dict_index), index_to_str([seq[-1]],dict_index)))


def create_model(max_sequence_len, total_words):
    input_len = max_sequence_len - 1
    model = Sequential()

    # Add Input Embedding Layer
    model.add(Embedding(total_words, 100, input_length=input_len))

    # Add Hidden Layer 1 - LSTM Layer
    model.add(LSTM(250))
    model.add(Dropout(0.3))

    # Add Output Layer
    model.add(Dense(total_words, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam')

    return model


def generate_text(seed_text, next_words, model, max_sequence_len, dict_word, dict_index):
    seed_text = seed_text.lower()
    for _ in range(next_words):
        # token_list = tokenizer.texts_to_sequences([seed_text])[0]
        token_list = line_to_index(seed_text.split(" "), dict_word)
        token_list = pad_sequences([token_list], maxlen=max_sequence_len - 1, padding='pre')
        predicted = model.predict_classes(token_list, verbose=0)
        output_word = dict_index[int(predicted)]
        seed_text += " " + output_word
    return seed_text.title()


def save_tokenizer(tokenizer, path):
    with open(path + 'tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load_tokenizer(path):
    with open(path, 'rb') as handle:
        tokenizer = pickle.load(handle)
    return tokenizer


def test_model(weight_path, tokenizer_path):
    tokenizer = load_tokenizer(path=tokenizer_path)
    model = create_model()


if __name__ == '__main__':
    data_train = load_data_train(PATH)
    corpus = [clean_text(x) for x in data_train]
    dict_word, dict_index = process_data(corpus)
    inp_sequences, total_words = get_sequence_of_tokens(corpus, dict_word)
    # prin_data_train(inp_sequences,dict_index)

    predictors, label, max_sequence_len = generate_padded_sequences(inp_sequences, total_words)
    print(len(label))
    model = create_model(max_sequence_len, total_words)
    # print(max_sequence_len)
    # model.summary()
    # model.fit(predictors, label, epochs=200, verbose=2)
    # model.save_weights("/home/aimenext/cuongdx/master_hust/weights/weight.h5")
    len_sentence = 15
    model.load_weights("weights/weight.h5")
    for i in range(10):
        len_inp=random.randint(1,4)
        input = " ".join([dict_index[idx] for idx in range(len_inp)]).upper()
        rs = generate_text(input, 15, model, max_sequence_len, dict_word, dict_index)
        print("Input Sentence: ",input)
        print("Predicted Sentence: ",rs.upper())
